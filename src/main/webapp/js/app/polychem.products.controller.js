
(function () {
    'use strict';
    angular
            .module('polychemApp')
            .controller('productsCtrl', mainFunction);

    mainFunction.$inject = ['$http', '$state'];

    function mainFunction($http, $state) {
        var products = this;
        products.list;
        getProductList();
        function getProductList() {
            return $http.get("js/models/products.json")
                    .then(performGetBuildComplete)
                    .catch(performGetBuildFailed);

            function performGetBuildComplete(response) {
                products.list = response.data;
                return response.data;
            }

            function performGetBuildFailed(error) {
                console.log('XHR Failed for login.' + error.data);
            }
        }

        products.displaySubProducts = function (index) {
//            products.showSubProducts = true;
//            products.productIndex = index;
            $state.go('productsDetails', {'productId': index});
        };
    }
})();