/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function () {
    'use strict';
    angular
            .module('polychemApp')
            .controller('HomePageCtrl', mainFunction);

    mainFunction.$inject = ['$scope'];

    function mainFunction($scope) {
        var vm = this;
        vm.myInterval = 3000;
        vm.noWrapSlides = false;
        vm.slides = [
            {
                "image": "images/slide1.jpg",
                "desc":"Superior Quality Plastic Products."
            },
            {
                "image": "images/slide2.jpg",
                "desc":"High-Tech godowns located all over Maharshtra."
            },
            {
                "image": "images/slide3.jpg",
                "desc":"PVC,LDPE,HDPE,PP,LLDPE"
            }
        ];
    }

})();
