
(function () {
    'use strict';
    angular
            .module('polychemApp')
            .controller('subProductsCtrl', mainFunction);

    mainFunction.$inject = ['$http', '$stateParams'];

    function mainFunction($http, $stateParams) {
        var products = this;
        products.list;
        getProductList();
        function getProductList() {
            return $http.get("js/models/products.json")
                    .then(performGetBuildComplete)
                    .catch(performGetBuildFailed);

            function performGetBuildComplete(response) {
                products.list = response.data;
                products.productIndex = $stateParams.productId;
                return response.data;
            }

            function performGetBuildFailed(error) {
                console.log('XHR Failed for login.' + error.data);
            }
        }
    }
})();