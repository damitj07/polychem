(function () {
    'use strict';
    angular
            .module('polychemApp')
            .controller('contactUsCtrl', mainFunction);

    mainFunction.$inject = ['$scope'];

    function mainFunction($scope) {
        var vm = this;
        vm.message = " My about us page.";
    }

})();
