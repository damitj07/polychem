/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function () {
    'use strict';
    angular
            .module('polychemApp')
            .run(appRun);

    /* @ngInject */
    appRun.$inject = ['routerHelper'];

    function appRun(routerHelper) {
        routerHelper.configureStates(getStates(), "/home");
    }

    function getStates() {

        return [
            {
                state: 'home',
                config: {
                    url: '/home',
                    templateUrl: 'pages/landing-page.html',
                    controller: 'HomePageCtrl',
                    controllerAs: 'vm'
                }
            },
            {
                state: 'about',
                config: {
                    url: '/about-us',
                    templateUrl: 'pages/about-us.html',
                    controller: 'aboutCtrl',
                    controllerAs: 'about'
                }
            },
            {
                state: 'privacy',
                config: {
                    url: '/privacy',
                    templateUrl: 'pages/privacy.html'
                }
            },
            {
                state: 'terms',
                config: {
                    url: '/terms',
                    templateUrl: 'pages/terms.html'
                }
            },
            {
                state: 'faq',
                config: {
                    url: '/faq',
                    templateUrl: 'pages/faq.html'
                }
            },
            {
                state: 'careers',
                config: {
                    url: '/careers',
                    templateUrl: 'pages/career.html',
                    controller: 'careersCtrl',
                    controllerAs: 'careers'
                }
            },
            {
                state: 'contactUs',
                config: {
                    url: '/contact-us',
                    templateUrl: 'pages/contact-us.html',
                    controller: 'contactUsCtrl'
                }
            },
            {
                state: 'products',
                config: {
                    url: '/products',
                    templateUrl: 'pages/products-library.html',
                    controller: 'productsCtrl',
                    controllerAs: 'products'
                }
            },
            {
                state: 'productsDetails',
                config: {
                    url: '/products/:productId',
                    templateUrl: 'pages/product-details.html',
                    controller: 'subProductsCtrl',
                    controllerAs: 'products'
                }
            }
        ];
    }

})();